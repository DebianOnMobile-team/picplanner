/*
 * milky-way-view.c
 * Copyright (C) 2021 Zwarf <zwarf@mail.de>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


/* TODO:
 * Visibility feature
 */

#include "milky-way-view.h"
#include <glib/gi18n.h>


struct _PicplannerMilkyway
{
  GtkBox parent_instance;

  GtkWidget *label_rise_time;
  GtkWidget *label_rise_azimuth;
  GtkWidget *label_upper_time;
  GtkWidget *label_upper_azimuth;
  GtkWidget *label_upper_elevation;
  GtkWidget *label_set_time;
  GtkWidget *label_set_azimuth;
  GtkWidget *label_disturbance_sun;
  GtkWidget *label_possible_disturbance_sun;
  GtkWidget *label_disturbance_moon;
  GtkWidget *label_visibility_time;
};

G_DEFINE_TYPE (PicplannerMilkyway, picplanner_milkyway, GTK_TYPE_BOX)


/*
 * Set the index of the rise, upper culmination and set
 * to present the information in the sun view.
 */
void
picplanner_milky_way_set_rise_upper_set (PicplannerMilkyway *milky_way,
                                         GDateTime *date_time,
                                         double *coordinates_array,
                                         int *index_rise_upper_set)
{
  char *char_rise_time;
  char *char_rise_azimuth;
  char *char_upper_time;
  char *char_upper_azimuth;
  char *char_upper_elevation;
  char *char_set_time;
  char *char_set_azimuth;
  GDateTime *date_time_rise = g_date_time_new_now_local ();
  GDateTime *date_time_upper = g_date_time_new_now_local ();
  GDateTime *date_time_set = g_date_time_new_now_local ();

	picplanner_get_date_time_from_index(&date_time_rise, &date_time, index_rise_upper_set[0]);
	picplanner_get_date_time_from_index(&date_time_upper, &date_time, index_rise_upper_set[1]);
	picplanner_get_date_time_from_index(&date_time_set, &date_time, index_rise_upper_set[2]);

    /*
   * Rise
   */
  if (index_rise_upper_set[0]>0)
    {
      char_rise_time = g_strdup_printf ("%02d:%02d",
                                        g_date_time_get_hour (date_time_rise),
                                        g_date_time_get_minute (date_time_rise));
      char_rise_azimuth = g_strdup_printf ("%s: %.0f\u00B0", _("Azimuth"),
                                           coordinates_array[index_rise_upper_set[0]*2]);
    }
  else
    {
      char_rise_time = g_strdup_printf ("--:--");
      char_rise_azimuth = g_strdup_printf ("%s: -\u00B0", _("Azimuth"));
    }

  /*
   * Upper culmination
   */
  char_upper_time = g_strdup_printf ("%02d:%02d",
                                     g_date_time_get_hour (date_time_upper),
                                     g_date_time_get_minute (date_time_upper));
  char_upper_azimuth = g_strdup_printf ("%s: %.0f\u00B0", _("Azimuth"),
                                        coordinates_array[index_rise_upper_set[1]*2]);
  char_upper_elevation = g_strdup_printf ("%s: %.0f\u00B0", _("Elevation"),
                                        coordinates_array[index_rise_upper_set[1]*2+1]);

  /*
   * Set
   */
  if (index_rise_upper_set[2]>0)
    {
      char_set_time = g_strdup_printf ("%02d:%02d",
                                       g_date_time_get_hour (date_time_set),
                                       g_date_time_get_minute (date_time_set));
      char_set_azimuth = g_strdup_printf ("%s: %.0f\u00B0", _("Azimuth"),
                                          coordinates_array[index_rise_upper_set[2]*2]);
    }
  else
    {
      char_set_time = g_strdup_printf ("--:--");
      char_set_azimuth = g_strdup_printf ("%s: -\u00B0", _("Azimuth"));
    }

  gtk_label_set_text (GTK_LABEL (milky_way->label_rise_time), char_rise_time);
  gtk_label_set_text (GTK_LABEL (milky_way->label_rise_azimuth), char_rise_azimuth);
  gtk_label_set_text (GTK_LABEL (milky_way->label_upper_time), char_upper_time);
  gtk_label_set_text (GTK_LABEL (milky_way->label_upper_azimuth), char_upper_azimuth);
  gtk_label_set_text (GTK_LABEL (milky_way->label_upper_elevation), char_upper_elevation);
  gtk_label_set_text (GTK_LABEL (milky_way->label_set_time), char_set_time);
  gtk_label_set_text (GTK_LABEL (milky_way->label_set_azimuth), char_set_azimuth);

  g_date_time_unref (date_time_rise);
  g_date_time_unref (date_time_upper);
  g_date_time_unref (date_time_set);
}


/*
 * Showes from when to when the milky way is visible on the milky way page.
 */
void
picplanner_milky_way_set_disturbance (PicplannerMilkyway 	*milky_way,
                                      GDateTime 					*date_time_noon,
																			double 							*coordinates_array_sun,
																			double 							*coordinates_array_moon,
																			double 							*coordinates_array_milky_way)
{
  char *char_label_disturbance_sun;
  char *char_label_disturbance_moon;
  char *char_label_no_disturbance;
  char *char_label_possibly_no_disturbance;


  char_label_disturbance_sun = picplanner_get_char_disturbance (date_time_noon,
                                                                -18, 360, -360,
                                                                coordinates_array_sun,
                                                                coordinates_array_moon,
                                                                coordinates_array_milky_way);

  gtk_label_set_text (GTK_LABEL (milky_way->label_disturbance_sun), char_label_disturbance_sun);

  char_label_disturbance_moon = picplanner_get_char_disturbance (date_time_noon,
                                                                 360, 0, -360,
                                                                 coordinates_array_sun,
                                                                 coordinates_array_moon,
                                                                 coordinates_array_milky_way);

  gtk_label_set_text (GTK_LABEL (milky_way->label_disturbance_moon), char_label_disturbance_moon);

  char_label_no_disturbance = picplanner_get_char_disturbance(date_time_noon,
                                                              -18, 0, 0,
                                                              coordinates_array_sun,
                                                              coordinates_array_moon,
                                                              coordinates_array_milky_way);

  gtk_label_set_text (GTK_LABEL (milky_way->label_visibility_time), char_label_no_disturbance);

  char_label_possibly_no_disturbance = picplanner_get_char_disturbance (date_time_noon,
                                                                        -12, 360, 0,
                                                                        coordinates_array_sun,
                                                                        coordinates_array_moon,
                                                                        coordinates_array_milky_way);

  gtk_label_set_text (GTK_LABEL (milky_way->label_possible_disturbance_sun), char_label_possibly_no_disturbance);

  g_free (char_label_disturbance_sun);
  g_free (char_label_disturbance_moon);
  g_free (char_label_no_disturbance);
  g_free (char_label_possibly_no_disturbance);
}


static void
picplanner_milkyway_init (PicplannerMilkyway *self)
{
  gtk_widget_init_template (GTK_WIDGET (self));
}

static void
picplanner_milkyway_class_init (PicplannerMilkywayClass *class)
{
  gtk_widget_class_set_template_from_resource (GTK_WIDGET_CLASS (class),
                                               "/de/zwarf/picplanner/window/milky-way-page/milky-way-view.ui");
  gtk_widget_class_bind_template_child (GTK_WIDGET_CLASS (class), PicplannerMilkyway, label_rise_time);
  gtk_widget_class_bind_template_child (GTK_WIDGET_CLASS (class), PicplannerMilkyway, label_rise_azimuth);
  gtk_widget_class_bind_template_child (GTK_WIDGET_CLASS (class), PicplannerMilkyway, label_upper_time);
  gtk_widget_class_bind_template_child (GTK_WIDGET_CLASS (class), PicplannerMilkyway, label_upper_azimuth);
  gtk_widget_class_bind_template_child (GTK_WIDGET_CLASS (class), PicplannerMilkyway, label_upper_elevation);
  gtk_widget_class_bind_template_child (GTK_WIDGET_CLASS (class), PicplannerMilkyway, label_set_time);
  gtk_widget_class_bind_template_child (GTK_WIDGET_CLASS (class), PicplannerMilkyway, label_set_azimuth);
  gtk_widget_class_bind_template_child (GTK_WIDGET_CLASS (class), PicplannerMilkyway, label_disturbance_sun);
  gtk_widget_class_bind_template_child (GTK_WIDGET_CLASS (class), PicplannerMilkyway, label_possible_disturbance_sun);
  gtk_widget_class_bind_template_child (GTK_WIDGET_CLASS (class), PicplannerMilkyway, label_disturbance_moon);
  gtk_widget_class_bind_template_child (GTK_WIDGET_CLASS (class), PicplannerMilkyway, label_visibility_time);
}

PicplannerMilkyway *
picplanner_milkyway_new ()
{
  return g_object_new (PICPLANNER_MILKYWAY_TYPE, NULL);
}
